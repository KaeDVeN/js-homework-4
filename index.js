"use strict"

let number1;
let number2;

do{
    number1 = prompt('Введіть 1-ше число');
    number2 = prompt('Введіть 2-ге число');
    if(isNaN(number1) || isNaN(number2)){
        alert('Два або одне із чисел не є числом, введіть ще раз');
    }
}while(isNaN(number1) || isNaN(number2));

if(number1 > number2){
    for(let i = number2; i <= number1; i++){
        alert(i);
    }
}else{
    for(let i = number1; i <= number2; i++){
        alert(i);
    }
}



// let userNumber;
//
// do{
//     userNumber = +prompt('Введіть парне число');
//     if (userNumber%2 !== 0 || userNumber === 0){
//         alert('Число яке ви ввели не є парним, введіть нове число');
//     }
//     console.log(userNumber);
// }while(userNumber%2 !== 0 || userNumber === 0);